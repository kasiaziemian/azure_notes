Azure networking 
- connect cloud and on-premises
- on-premise networking functionality


azure virtual network
- logically isolated networking components 
- segmented into one or more subnets 
- subnets are discrete sections
- enable communication of resources with each-other, internet and on-premise
- scoped to a single region 
- VNet peering allow cross region communication 
- isolation, segmentation, communication, filtering and routing


azure load balancer 
- even traffic distribution 
- supports both inbound and outbound scenarios 
- high-availability scenarios 
- both TCP and UDP applications
- port forwaring 
- high scale with up to millions of flows 


vpn gateway
- specific type of virtual network gateway for on-premises to azure traffic over the public internet 


application gateway 
- web traffic load balancer
- web application firewall
- redirection 
- session afinity
- url routing 
- ssl termination 


content delivery network
- define content 
- minimize latency 
- points of presence (pop) with many locations 