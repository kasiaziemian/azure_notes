## virtualization 
- emulation of psychical machines 
- different virtual hardware configuration per machine/app
- different operating system per machine/app 
- total separation of environments 
    - file systems 
    - services 
    - ports
    - middleware
    - configuration 


## virtual machines 
- infrastucture as a service (IaaS)
- total control over the operating system and software 
- supports marketplace and custom images 
- best suited for 
    - custom software requiring custom software configuration 
    - lift-and-shift scenarios
- can run any application/scenario 
    - web and app / web services
    - databases
    - desktop applications 
    - jumboxes 
    - gateways 


## virtual machine scale sets 
- infrastucture as a service (IaaS)
- set of inentical virtual machines 
- built-in auto scaling features
- designed for manual and auto-scaled workloads like web servers, batch proccesing 


## containers 
- use host's operating system
- emulate operating system (VMs emulate hardware)
- lighweight (no O/S)
    - development effort
    - maintenance
    - compute and storage requirements 
- respond quicker to demand changes 
- designed for almost any scenario



## azure kubernetes service (AKS)
- open-source Container orchastration platform 
- platform as a services (PaaS)
- highly scalable and customizable 
- designed for high scale container deployments



## app service 
- designed as enterprise grade web application service
- platform as a service (PaaS)
- supports multiple programming languages and Containers


## azure functions (Function App)
- platform as a service (PaaS)
- serverless
- two hosting/pricing models
    - consumption-based plan 
    - dedacated plan
- designed for micro/nano-services

---

### virtual machines (IaaS)
custom software, custom requirements, very specialized, high degree of control
### vm scale set (IaaS)
 auto-scaled workloads for vms 
### container instances (PaaS)
simple container hosting, easy to start 
### kubernetes service (PaaS)
highly scalable and customizable container, hosting platform
### app service (PaaS)
web applications, a lot of enterprise web hosting features, easy to start
### functions (PaaS)
micro/nano services, excellent consuption based pricing, easy to start 