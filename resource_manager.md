## azure resource
- object use to manage services in Azure 
- saved as Json
- represents service lifecycle 

## resource groups
- grouping of resources 
- holds logically related resources 
- typically organzing by 
    - type
    - lifecycle 
    - department
    - billing 
    - location 
    - combination of those 

## resource manager 
- managememnt layer for all resources and only one resource group
- unified language
- controls access and resources 