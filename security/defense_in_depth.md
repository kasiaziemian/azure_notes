7 layers of security 

1. data - access to business and customer data and encryption to protect data
2. application - applications are secure and free of security vulnerabilities
3. compute - access to virtual machines (ports, on-premises, cloud)
4. network - limit communication between resources using segmentation and access control
5. perimeter - distributed denial of service (ddos) protection to filter large-scale attacks before they can cause a denial 
   of service for users
6. identity and access - controlling access to infrastucture and change control
7. phiscial - limiting access to datacenter to only authorize personnel