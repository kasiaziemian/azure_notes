confidentiality, integrity and availability triad is a mode descibing the foundation to security principles and their trade-off relationship

1. confidentiality - component of privacy that implements to protect customer data form anauthorized viewers, in practice this can be using cryptography keys to encrypt data and using keys to encyrpt customer keys (envelope encryption)

2. integrity - maintaining and assuring the accuracy and completeness of data over its entire lifecycle, in practice utilizing acid compliant databases and valid transactions, utilizing temper-evident of tamper proof hardware security modules (hsm)

3. availability - information needs to be made be available when needed, in practice high availability, migrating ddos, decryption access
