### thread 
 in cloud security is a potential negative action or event facilitated a vulnerability that results in an unwanted impact to a computer system or application


## 4 types of threads:
1. dictionary attack - attacker attemps to steal an identity by bruteforcing into 
a target accounts by enumerating over a large number of known passwords

2. ransmware - a type of malicious software (malware) that when installed holds data, workstation or a network hostage until the ransom has been paid

3. disruptive attacks - an attack which attemps to disrupt a computer system or computer network for various reasons: ddos, coin miners, rootkits, trojans, worms, etc 

4. data breach - when a malicious actor gains unauthorized acces to a system in order to extract private data 

