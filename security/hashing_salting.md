## hashing function 
a function that accepts arbitrary size value and maps it to fixed-size data structure, hashing can reduce the size of the store value 


hashing is a one-way process and is deterministic,
a deterministic function always returns the same output for the same input 

## hashing passwords 

hashing functions are used to store password in database so that a password does not reside a plaintext format

to authenticate a user, when a user inputs their password, it is hashed and the hash is compared to the stored hashed 

popular hashing functions:
#### MD5 
#### SHA356
#### Bcrypt
---

if an attacker knowns what function you are using and stole your database, they could enumerate a dictionary of password to determine the password 

## salting passwords 
a salt is a random string not known to the attacker that the hash function accepts to mitigate the deterministic nature of hashing functions 

