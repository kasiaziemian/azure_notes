# zero trust model 

## principles

1. verify explicity 
- always authenticate and authorize based on on all available data points
2. least priviliged access 
- limit users access with just-in-time and just-enough-access (jit/jea), risk based adaptive policies and data protection 
3. assume breach 
- minimize blast radius and segment access, verify end-to-end encryption and use analitycs to get visibility, drive threat detection and imporve defenses


## pillars 

### identities 
- verify and secure each identity with strong authentication across your entire digital estate
    - identity access and management (IAM)
    - azure active directory 
    - single-sign on
    - multi-factor authentication 
    - passwordless authentication  
    - risk-based policies
    - identity secure score 


endpoints (devices)
- gain visibility into devices accessing the network, ensure compliance and health status before granting access
    - register devices to IpD (azure ad device management)
    - microsoft intune (mdm and mam)
    - microsoft endpoint manager 
    - microsoft defender for endpoints
    - data loss prevention (DLP) policies

apps 
- discover shadow IT, ensure appropriate in-app permissions, gate access based on real-time analitycs and monitor and control user 
    - policy-based access controls 
    - microsoft cloud app security (mcas)
    - azure ad application proxy 
    - cloud discovery
    - jit vm access 

data 
- move from perimeter-based data protection to data-driven protection, use intelligence to classify and label data, encrypt and restrict access based on organization policies 
    - sensivity labels 
    - microsoft information protection 
    - data classification
    - azure information protection (api) scanner 
    - decision based policies 
    - data loss prevention (DLP) policies 

infrastucture
- use telemetry to detect attacks and anomalies automatically block and flag risky behavior and employ least privilige access principles
    - azure security center 
    - azure ad managed identities
    - user and resource segmentation
    - vnets 
    - peering rules
    - priviliged identity management
    - network security groups (nsg)
    - application security groups (asg)
    - azure firewall
    - microsoft defender for endpoints 
    - microsoft defender for identity 
    - azure sentinel 

networks 
- ensure devices and users aren't trusted just because they're on a internal network, encrypt all internal communications, limit access by policy and employ microsegmentation and real-time thret detection
    - network segmentation
    - azure ddos protection service
    - azure firewall 
    - azure web application firewall (waf)
    - azure vpn 
    - azure ad proxy
    - azure bastion 
    - ssl/tls 




