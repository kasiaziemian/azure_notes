shared responsibility model descibes what the customer and azure is responsibility for related to cloud resources


### responsibility 
- information and data (responsibility always retained by customer) - SaaS, PaaS, IaaS, on-premise
- devices, mobiles and pcs (responsibility always retained by customer) - SaaS, PaaS, IaaS, on-premise
- accounts and identities (responsibility always retained by customer) - SaaS, PaaS, IaaS, on-premise
- identity and directory infrastucture (responsibility varies by service type) - SaaS, *PaaS, IaaS, on-premise
- applications (responsibility varies by service type) - *PaaS, IaaS, on-premise
- network controls - *PaaS, IaaS, on-premise
- operating sysstem (responsibility varies by service type) - IaaS, on-premise
- psychical hosts (responsibility transfers by cloud prividers) - on-premise
- psychical network (responsibility transfers by cloud providers) - on-premise
- psychical data center (responsibility transfers by cloud providers) - on-premise

1. software as a service (SaaS) - software that use in the cloud eg. microsoft 365, skype, dynamics customer
2. platform as a service (PaaS) - deploy apps without worrying about underlying infrastucture, azure app services
3. infrastucture as a service (IaaS) - basic builing blocks of cloud IT eg. storage, compute, databases, networking 
on-premise - datacenter owned, operated and maintained by customer 


regardless of the type of development, the following responsibility are always retained by customer
- data
- endpoints
- account 
- access management